namespace FoxCharms;

public class GlobalSettings
{
    public bool AddCharms = true;
    // transendence adds more settings here, a lot of them for randomizer compatibility
    public bool EncounteredWrithingShadow = false;
}