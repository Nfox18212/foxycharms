﻿
// thanks to dpinela's transcendence mod for the example code
using System;
using System.Collections.Generic;



namespace FoxCharms
{

    internal abstract class Charm 
    {
        public abstract string Sprite { get;  }
        public abstract string Name { get; }
        public abstract string Description { get;  }
        public abstract int DefaultCost { get;  }
        public abstract string Scene { get; }
        public abstract float XPos { get;  }
        public abstract float YPos { get; }
        
        // according dpinela, this is assigned at runtime by sfcore's charmhelper
        public int Num { get; set; } // what is this number?

        public bool Equipped() => PlayerData.instance.GetBool($"equippedCharm_{Num}");

        //public abstract CharmSettings Settings(SaveSettings s); // for saving settings?  something like that
        
        public virtual void Hook() {}
        public virtual List<(string obj, string fsm, Action<PlayMakerFSM> edit)> FsmEdits => new();
        public virtual List<(int Period, Action Func)> Tickers => new();

       // public abstract void MarkasEncountered(GlobalSettings s);
       // i have legit no idea why this is here
    }
        
};
    

        