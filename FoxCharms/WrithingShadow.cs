using System.Collections;
using UnityEngine;
using GlobalEnums;
using HutongGames.PlayMaker;
using HutongGames.PlayMaker.Actions;

namespace FoxCharms
{
    internal class WrithingShadow : Charm
    {
        public static readonly WrithingShadow Instance = new();
        public override string Sprite => "placeholder_charm.png";
        public override string Name => "Writhing Shadow";
        public override string Description => "An ancient charm that forces shadows to harm enemies.\n\nThe bearer's nail will deal damage over time at a cost to direct strength.";
        public override int DefaultCost => 2;
        public override string Scene => "Abyss_Lighthouse_Room";
        public override float XPos => 53.6f;
        public override float YPos => 23.4f;
        
        // dot effect
        public override void Hook()
        {
            On.DamageEffectTicker.Update += EditTickDamage;
        }

        private void EditTickDamage(On.DamageEffectTicker.orig_Update orig, DamageEffectTicker self) 
            // not sure what the difference between orig and self are for this method
        {
            var tickDmg = self.GetComponent<CustomTickDamage>();
        }
    }    
}

